window.devenv = true;
YUI().use('get', function (A) {
	var baseUrl = '/o/digitalplace-electric-theme/js/';
	A.Get.script([

		baseUrl + 'back-to-referrer.js',
		baseUrl + 'navigation.js',
		baseUrl + 'show-hide-toggle.js'

	],
		{
			onSuccess: function () { }
		}
	);
});

$(document).ready(function () {
	$('.portlet-search-bar .search-bar-suggestions .search-bar-keywords-input').prop("autocomplete", "off");
	$('.portlet-search-bar .search-bar-suggestions .search-bar-keywords-input').attr("role", "search");
});