AUI().ready(function (A) {
	
	function registerNavBarMouseEvents(){
		if (screen && screen.width > 768) {
			$(".navbar-site li.dropdown").hover(function (e) {
				var target = $(e.target);
				resizeChildDropdownMenu();
				$(target).closest("li.dropdown").addClass(["hover", "open"]);
			});
		
			$(".navbar-site li.dropdown").mouseleave(function (e) {
				var target = $(e.target);
		
				$(target).closest("li.dropdown").removeClass(["hover", "open"]);
			});
		}
	}

	var prevTarget;
	$(".dropdown-toggle").focus(function (e) {
		var target = $(e.target);

		$(target).closest("li.dropdown").addClass("open");

		prevTarget = target;

	});

	$(".dropdown-toggle").focusout(function (e) {
		closeDropdown(e);
	});

	$(".dropdown-item").focusout(function (e) {
		closeDropdown(e);
	});

	function closeDropdown(e) {
		var isNotDropdownItem = !$(e.relatedTarget).is(".dropdown-item");
		if (isNotDropdownItem) {
			$(prevTarget).closest("li.dropdown").removeClass("open");
			delete prevTarget;
		}
	}

	function resizeChildDropdownMenu() {
		$(".dropdown-menu-wrapper").each(function () {
			var headerNavOffsetLeft = $('#header-nav').offset().left;
			var navOffsetLeft = $('.navigation-wrapper').offset().left;
			var dropDownWindowWidth = $('#header-nav').width();
	
			var elementOffsetLeft = navOffsetLeft;

			if (headerNavOffsetLeft > 0) {
				elementOffsetLeft = navOffsetLeft - headerNavOffsetLeft;
			}
	
			$(this).css({ left: -Math.abs(elementOffsetLeft), width: dropDownWindowWidth, maxWidth: dropDownWindowWidth });
		});
	}

	A.on('resize', function(event) {
		resizeChildDropdownMenu();
		registerNavBarMouseEvents();
	});

	function isIE() {
		const ua = window.navigator.userAgent;
		const msie = ua.indexOf('MSIE '); // IE 10 or older
		const trident = ua.indexOf('Trident/'); //IE 11

		return (msie > 0 || trident > 0);
	}

	$("ul.child-menu.dropdown-menu").each(function () {
		var numberOfChildren = $(this).find(" > li").length;
		var heightPerElement = 20;

		if (numberOfChildren < 5) {
			heightPerElement = 50;
			$(this).find(" > li").addClass("col-50");
		}
		else if (numberOfChildren < 10) {
			heightPerElement = 30;
			$(this).find(" > li").addClass("col-50");
		}

		if (isIE()) {
			$(this).css({ height: numberOfChildren * heightPerElement });
		} else {
			$(this).css({ maxHeight: numberOfChildren * heightPerElement });
		}

	});

	$("button.navbar-toggler").on('click', function (e) {
		var button = $("button.navbar-toggler");

		if ($(button).attr('aria-expanded') == 'false') {
			$(button).find('.button-label span').text("Close");
		}
		else {
			$(button).find('.button-label span').text("Menu");
			$("li.nav-item").show();
			$("li.nav-item > a").show();
			$("li.nav-item.back-button").remove();
		}
	});

	$("li.nav-item.dropdown").on("click", function (e) {
		var currentTarget = $(e.currentTarget);
		var firstChild = this.firstElementChild;

		if (firstChild != null) {
			if (screen && screen.width < 768) {
				$(currentTarget).siblings("li.nav-item").hide();
				$(currentTarget).find("> a").hide();
				$(currentTarget).parent().prepend('<li class="lfr-nav-item nav-item back-button">Back</li>');

				$("li.nav-item.back-button").on("click", function (e) {
					var target = $(e.target);
	
					$(target).siblings("li.nav-item").show();
					$(target).siblings("li.nav-item").removeClass(["open", "hover"]);
					$(target).siblings("li.nav-item").find("> a").show();
					$(target).remove();
				});
			}

			if (!$(currentTarget).hasClass("open")) {
				$(currentTarget).addClass(["open", "hover"]);
			}
		}
	});

	resizeChildDropdownMenu();
	registerNavBarMouseEvents();
});