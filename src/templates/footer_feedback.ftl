<#assign form_background_class = "primary-background" />
<#assign show_btn_class = "feedbackShowHide" />
<#assign hide_btn_class = "feedbackShowHide d-none" />
<#assign collapsible_class = "collapse form-background" />

<#if portlet_id?has_content && portlet_id?contains("feedbackForm")>
	<#assign form_background_class = "success-background" />
	<#assign show_btn_class = "feedbackShowHide d-none" />
	<#assign hide_btn_class = "feedbackShowHide" />
	<#assign collapsible_class = "collapse form-background show" />

	<#assign form_submitted = true />
</#if>

<div class="footer-feedback">
	<div class="container container-panel ${form_background_class}">
		<div class="row">
			<div class="col-md-10 text-section">
				<#if form_submitted?has_content>
					<div class="success">
						<span class="title"><@liferay.language key="thank-you-for-your-feedback" /></span>
					</div>
				<#else>
					<span class="title"><@liferay.language key="share-your-feedback" /> <i class="icon-comment"></i></span>
					<p><@liferay.language_format arguments="3" key="please-answer-x-quick-questions" /></p>
				</#if>
			</div>
			<div class="col-md-2 show-hide-button">
				<a data-toggle="collapse" href="javascript:void(0);" data-target="#feedbackForm" aria-controls="feedbackForm">
						<span class="${show_btn_class}"><@liferay.language key="show" /> &#9652;</span>
						<span class="${hide_btn_class}"><@liferay.language key="hide" /> &#9662;</span>
				</a>
			</div>
		</div>
		<div class="${collapsible_class}" id="feedbackForm">
			<div>
				<#assign 
					instance_id = "feedbackForm_${group_id}_${feedback_form_id}"
					portlet_key = "com_liferay_dynamic_data_mapping_form_web_portlet_DDMFormPortlet"
					form_portlet_id = "${portlet_key}_INSTANCE_${instance_id}"
					defaultPreferences = "<preference><name>formInstanceId</name><value>${feedback_form_id}</value></preference>"
					VOID = portletPreferencesLocalService.getPreferences(company_id, group_id, 3, 0, form_portlet_id, defaultPreferences)
				/>
				<#assign customPortletPreferences=freeMarkerPortletPreferences.getPreferences({"formInstanceId": feedback_form_id}) />
				<@liferay_portlet["runtime"]
					instanceId="${instance_id}"
					portletName="${portlet_key}"
					defaultPreferences="${customPortletPreferences}"
				/>
			</div>
		</div>
	</div>
</div>
