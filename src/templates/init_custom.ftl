<#assign
	body_css_class = css_class
	signin_header_link = getterUtil.getString(theme_settings["signin-header-link"])
	myaccount_header_link = getterUtil.getString(theme_settings["myaccount-header-link"])
	feedback_footer_link = getterUtil.getString(theme_settings["feedback-footer-link"])
	feedback_form_id = getterUtil.getLong(theme_settings["feedback-form-id"], 0)
	gtm_id = getterUtil.getString(theme_settings["gtm-id"])
	primary_background_color = getterUtil.getString(theme_settings["primary-background-color"])
	primary_font_color = getterUtil.getString(theme_settings["primary-font-color"])
	show_breadcrumbs = getterUtil.getBoolean(theme_settings["show-breadcrumbs"])
	show_widget_page_title = getterUtil.getBoolean(theme_settings["show-widget-page-title"])
	show_navigation = getterUtil.getBoolean(theme_settings["show-navigation"])
	use_guest_navigation = getterUtil.getBoolean(theme_settings["use-guest-navigation"])
	show_translate = getterUtil.getBoolean(theme_settings["show-translate"])
	sign_in_url = htmlUtil.escape(theme_display.getURLSignIn())

	digitalPlacePermissionService = serviceLocator.findService("com.placecube.digitalplace.local.permission.service.DigitalPlacePermissionService")
	navigationMenuItemService = serviceLocator.findService("com.liferay.site.navigation.service.SiteNavigationMenuItemLocalService")
	navigationMenuService = serviceLocator.findService("com.liferay.site.navigation.service.SiteNavigationMenuLocalService")
	siteNavigationMenItemHelper = serviceLocator.findService("com.placecube.digitalplace.local.navigationmenu.helper.SiteNavigationMenItemHelper")
	portletPreferencesLocalService = serviceLocator.findService("com.liferay.portal.kernel.service.PortletPreferencesLocalService")
/>

<#if is_signed_in>
	<#assign account_header_link = myaccount_header_link />
	<#if theme_display.isImpersonated() >
		<#assign account_header_link = myaccount_header_link + "?doAsUserId=" + htmlUtil.escapeURL(theme_display.getDoAsUserId()) />
	</#if>
<#elseif signin_header_link?has_content>
	<#assign account_header_link = signin_header_link />
<#elseif sign_in_url??>
	<#assign account_header_link = sign_in_url />
</#if>

<#if !logo_description?has_content >
	<#assign logo_description = htmlUtil.escape(site_name) />
</#if>

<#assign page_heading = layout.getHTMLTitle(locale) />

<#if feedback_footer_link?has_content >
	<#assign content_css_class = content_css_class + " has-footer-feedback" />
</#if>

<#assign show_translate = show_translate && !is_signed_in />
