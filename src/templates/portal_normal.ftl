<!DOCTYPE html>

<#include init />

<html class="${root_css_class}" dir="<@liferay.language key="lang.dir" />" lang="${w3c_language_id}">

	<head>
		<title>${the_title} - ${company_name}</title>

		<meta content="initial-scale=1.0, width=device-width" name="viewport" />

		<#if gtm_id?has_content>
			<script type="text/javascript">
				(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
				new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
				j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
				'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
				})(window,document,'script','dataLayer','${gtm_id}');
			</script>
		</#if>

		<style>
			.primary-background {
				background-color: ${primary_background_color};
			}
			.primary-font-color {
				color: ${primary_font_color};
			}
		</style>

		<@liferay_util["include"] page=top_head_include />

	</head>

	<body class="${body_css_class!''}">

		<@liferay_ui["quick-access"] contentId="#main-content" />

		<#if gtm_id?has_content>
			<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=${gtm_id}" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		</#if>

		<@liferay_util["include"] page=body_top_include />

		<@liferay.control_menu />

		<div id="wrapper" class="digitalplace-wrapper">

			<#include "${full_templates_path}/header.ftl" />

			<section id="content" class="${content_css_class!''}">

				<#if is_portlet_page && show_widget_page_title>
					<h1>${page_heading}</h1>
				</#if>

				<#if selectable>
					<@liferay_util["include"] page=content_include />
				<#else>
					${portletDisplay.recycle()}
					${portletDisplay.setTitle(the_title)}

					<@liferay_theme["wrap-portlet"] page="portlet.ftl">
						<@liferay_util["include"] page=content_include />
					</@>
				</#if>
			</section>

			<#include "${full_templates_path}/footer.ftl" />
		</div>

		<#if show_translate>
			<script type="text/javascript">
				function googleTranslateElementInit() {
				  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.HORIZONTAL}, 'google_translate_element');
				}
			</script>
			<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
		</#if>

		<@liferay_util["include"] page=body_bottom_include />
		<@liferay_util["include"] page=bottom_include />

		<!-- inject:js -->
		<!-- endinject -->

	</body>

</html>
