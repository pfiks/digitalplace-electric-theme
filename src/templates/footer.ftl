<footer id="footer" role="contentinfo" class="site-footer">

	<#if feedback_form_id gt 0 >
		<#include "${full_templates_path}/footer_feedback.ftl" />
	</#if>

	<div class="background-wrapper">
		<div class="footer-content-navigation">

			<div class="container">

				<#assign menus = navigationMenuService.getSiteNavigationMenus(theme_display.scopeGroupId) />
				<div class="row">
					<#list menus as menu>
						<#assign menu_id = menu.getSiteNavigationMenuId() />

						<#if siteNavigationMenItemHelper.isSecondaryNavigation(menu) >
							<#assign menu_id = menu.getSiteNavigationMenuId() />
							<#assign menu_items_raw = navigationMenuItemService.getSiteNavigationMenuItems(menu_id) />
							<#assign menu_items = siteNavigationMenItemHelper.getSortedSiteNavigationMenuItems(menu_items_raw) />
							<#if menu_items[0..*4]?has_content >
								<div class="col-md-4">
									<div class="footer-nav-secondary">
										<ul>
											<#list menu_items[0..*4] as menu_item>
												<#assign
													properties = siteNavigationMenItemHelper.getSiteNavigationMenuItemProperties(menu_item)
													name = siteNavigationMenItemHelper.getMenuItemName(properties, themeDisplay)
													url = siteNavigationMenItemHelper.getMenuItemUrl(menu_item, properties, themeDisplay)
												/>
												<#if siteNavigationMenItemHelper.isRedirectToCurrentWebsite(layoutSet, url)>
													<#assign menu_item_target = ""/>
												<#else>
													<#assign menu_item_target = "_blank"/>
												</#if>
												<li>
													<a href="${ url }" target="${ menu_item_target }">${ name }</a>
												</li>
											</#list>
										</ul>
									</div>
								</div>
							</#if>

							<div class="col-md-4">
								<#if menu_items?size gt 4 >
									<div class="footer-nav-secondary">
										<ul>
											<#list menu_items[4..*4] as menu_item>
												<#assign
													properties = siteNavigationMenItemHelper.getSiteNavigationMenuItemProperties(menu_item)
													name = siteNavigationMenItemHelper.getMenuItemName(properties, themeDisplay)
													url = siteNavigationMenItemHelper.getMenuItemUrl(menu_item, properties, themeDisplay)
												/>
												<#if siteNavigationMenItemHelper.isRedirectToCurrentWebsite(layoutSet, url)>
													<#assign menu_item_target = ""/>
												<#else>
													<#assign menu_item_target = "_blank"/>
												</#if>
												<li>
													<a href="${ url }" target="${ menu_item_target!"" }">${ name }</a>
												</li>
											</#list>
										</ul>
									</div>
								</#if>
							</div>
						</#if>

						<#if siteNavigationMenItemHelper.isSocialNavigation(menu) >
							<div class="col-md-4">
								<#assign menu_items_raw = navigationMenuItemService.getSiteNavigationMenuItems(menu_id) />
								<#assign menu_items = siteNavigationMenItemHelper.getSortedSiteNavigationMenuItems(menu_items_raw) />

								<#if menu_items_raw?has_content>

									<div class="footer-nav-social">
										<div class="follow-title">Follow us:</div>
										<ul>
											<#list menu_items as menu_item>
												<#assign properties = siteNavigationMenItemHelper.getSiteNavigationMenuItemProperties(menu_item)>
												<#assign name = siteNavigationMenItemHelper.getMenuItemName(properties, themeDisplay)>
												<#assign url = siteNavigationMenItemHelper.getMenuItemUrl(menu_item, properties, themeDisplay)?lower_case>

												<#assign socialCssClass = 'sign-blank'>
												<#if url?contains('facebook')><#assign socialCssClass = 'facebook-sign'></#if>
												<#if url?contains('youtube')><#assign socialCssClass = 'youtube-play'></#if>
												<#if url?contains('twitter')><#assign socialCssClass = 'twitter'></#if>
												<#if url?contains('instagram')><#assign socialCssClass = 'instagram'></#if>
												<#if url?contains('flickr')><#assign socialCssClass = 'flickr'></#if>
												<li>
													<span class="icon-${ socialCssClass }" >
														<a href="${ url }" target="_blank">
															${ name }
														</a>
													</span>
												</li>
											</#list>
										</ul>
									</div>

								</#if>
							</div>
						</#if>

					</#list>
				</div>

				<div class="row">
					<div class="col-md-2">
						<div class="footer-logo">
							<a class="${logo_css_class}" href="${site_default_public_url}" title="<@liferay.language_format arguments="${site_name}" key="go-to-x" />">
								<img alt="${logo_description}" src="${site_logo}" />
							</a>
						</div>
					</div>
					<div class="col-md-10">
						<p class="footer-rights">Copyright © ${.now?string('yyyy')} ${company_name}</p>
					</div>

				</div>

			</div>
		</div>
	</div>
</footer>
