<header id="dp-header" role="banner">

	<#assign 
		preferences = freeMarkerPortletPreferences.getPreferences({"portletSetupPortletDecoratorId": "barebone", "destination": "/search"})
		layoutLocalService = serviceLocator.findService("com.liferay.portal.kernel.service.LayoutLocalService")
		publicLayouts = layoutLocalService.getLayouts(group_id, false)
		groupLocalService = serviceLocator.findService("com.liferay.portal.kernel.service.GroupLocalService")
		guestGroup = groupLocalService.getGroup(company_id, 'Guest')
		scopeGroup = groupLocalService.getGroup(group_id)
	/>

	<#if use_guest_navigation>
		<#assign navigationGroupId = (guestGroup.isStaged() && guestGroup.hasStagingGroup() && scopeGroup.isStagingGroup())?then(guestGroup.getStagingGroup().getGroupId(), guestGroup.getGroupId()) />
	<#else>
		<#assign navigationGroupId = (scopeGroup.isStaged() && scopeGroup.isStagingGroup())?then(scopeGroup.getStagingGroup().getGroupId(), scopeGroup.getGroupId()) />
	</#if>

	<#assign headerMenu = navigationMenuService.fetchPrimarySiteNavigationMenu(navigationGroupId)!/>

	<#if headerMenu?has_content>
		<#assign
			headerMenuId = headerMenu.getSiteNavigationMenuId()
			headerParentMenuItems = navigationMenuItemService.getSiteNavigationMenuItems(headerMenuId, 0)
		/>
	<#else>
		<#assign headerParentMenuItems = [] />
	</#if>

	<#if show_translate>
		<div id="header-translate">
			<div class="container">
				<div id="google_translate_element"></div>
			</div>
		</div>
	</#if>

	<#if show_navigation>
		<div id="header-nav">
			<div class="container">
				<div class="header-content col-md-12">

					<#if show_site_name>
						<div class="site-logo">
							<a class="${logo_css_class} header-logo" href="${site_default_public_url}" title="<@liferay.language_format arguments="${site_name}" key="go-to-x" />">
								<img alt="${logo_description}" src="${site_logo}" />
							</a>
						</div>
					</#if>

					<#if has_navigation && is_setup_complete>
						<button aria-controls="navigationCollapse" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler navbar-toggler-right" data-target="#navigationCollapse" data-toggle="collapse" type="button">
							<div class="button-part burguer">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</div>
							<div class="button-part cross-button"><@clay["icon"] symbol="times" /></div>
							<div class="button-part button-label">
								<span>Menu</span>
							</div>
						</button>

						<div aria-expanded="false" class="collapse navbar-collapse" id="navigationCollapse">
							<div class="login-row">
							<#if account_header_link??>
								<#if !is_signed_in>
									<a href="${account_header_link}"><@clay["icon"] symbol="user" /><span>Log in to my account</span></a>
								<#else>
									<a class="log-in-link" href="${account_header_link}"><@clay["icon"] symbol="user" /><span>Your account</span></a>
								</#if>
							</#if>
							</div> 
							<nav class="navigation-wrapper" aria-label="Site Pages">
								<ul class="navbar-blank navbar-nav navbar-site" role="menubar">
									<#if headerParentMenuItems?has_content>
										<#list headerParentMenuItems as headerParentMenuItem>

											<#assign
												nav_item_css_class = "lfr-nav-item nav-item"
												link_css_class = "nav-link text-truncate"
												properties = siteNavigationMenItemHelper.getSiteNavigationMenuItemProperties(headerParentMenuItem)
											/>

											<#if !(headerParentMenuItem.getType() == "layout")>
												<#assign
													menu_item_name = siteNavigationMenItemHelper.getMenuItemName(properties, theme_display)!""
													menu_item_url = siteNavigationMenItemHelper.getMenuItemUrl(headerParentMenuItem, properties, theme_display)!""
												/>

												<#if siteNavigationMenItemHelper.isRedirectToCurrentWebsite(layoutSet, menu_item_url)>
													<#assign menu_item_target = ""/>
												<#else>
													<#assign menu_item_target = "_blank"/>
												</#if>

												<li class="${nav_item_css_class}" role="presentation">
													<a class="${link_css_class}" href="${menu_item_url}" target="${menu_item_target}" role="menuitem">
														<span class="text-truncate" role="presentation">${menu_item_name}</span>
													</a>
												</li>

											<#else>

												<#assign optionalNavigationEntry = siteNavigationMenItemHelper.getMenuItemLayout(headerParentMenuItem, properties) />

												<#if optionalNavigationEntry.isPresent()>
													<#assign navigationEntry = optionalNavigationEntry.get() />

													<#if !navigationEntry.isHidden() && layoutPermission.contains(theme_display.getPermissionChecker(), navigationEntry.getPlid(), 'VIEW')>

														<#if navigationEntry.isSelected(theme_display.isTilesSelectable(), theme_display.getLayout(), theme_display.getLayout().getAncestorPlid())>
															<#assign
																nav_item_css_class = "lfr-nav-item nav-item selected active"
																link_css_class = "nav-link text-truncate active"
															/>
														</#if>

														<#if !navigationEntry.hasChildren()>
															<#assign
																li_has_children = ""
																link_has_children = ""
															>
														<#else>
															<#assign
																li_has_children = "dropdown"
																link_has_children = "dropdown-toggle"
																link_css_class = "nav-link text-truncate"
															>
														</#if>

														<li class="${nav_item_css_class} ${li_has_children}" id="layout_${navigationEntry.getLayoutId()}" role="presentation">
															<#if navigationEntry.hasChildren()>
																<a aria-expanded="false" aria-haspopup="true" class="${link_css_class} ${link_has_children}" role="menuitem">
																	<span class="text-truncate" role="presentation">
																		${siteNavigationMenItemHelper.getMenuItemName(properties, theme_display)!navigationEntry.getName(locale)}

																		
																		<span class="lfr-nav-child-toggle" role="presentation"><@clay["icon"] symbol="angle-down" /></span>
																		

																	</span>
																</a>
															<#else>
															<a aria-expanded="false" aria-haspopup="true" class="${link_css_class} ${link_has_children}" href="${navigationEntry.getRegularURL(request)}" role="menuitem">
																	<span class="text-truncate" role="presentation">
																		${siteNavigationMenItemHelper.getMenuItemName(properties, theme_display)!navigationEntry.getName(locale)}
																	</span>
																</a>
															</#if>

															<#if navigationEntry.hasChildren()>
																<#assign childrenNavigationEntries = navigationEntry.getChildren(theme_display.getPermissionChecker())>

																<div class="dropdown-menu-wrapper container" role="presentation">
																	<div class="container" role="presentation">
																		<ul class="child-menu dropdown-menu" role="menu" aria-label="${siteNavigationMenItemHelper.getMenuItemName(properties, theme_display)!navigationEntry.getName(locale)}">

																			<#if childrenNavigationEntries?has_content>
																				<#list childrenNavigationEntries as childNavigationEntry>
																					<#if !childNavigationEntry.isHidden() && layoutPermission.contains(theme_display.getPermissionChecker(), childNavigationEntry.getPlid(), 'VIEW')>
																						<#assign
																							child_nav_item_css_class = ""
																							child_link_css_class = "dropdown-item"
																						/>

																						<#if childNavigationEntry.isSelected(theme_display.isTilesSelectable(), theme_display.getLayout(), theme_display.getLayout().getAncestorPlid())>
																							<#assign
																								child_nav_item_css_class = "selected active"
																								child_link_css_class = "dropdown-item active"
																							/>
																						</#if>

																						<li class="${child_nav_item_css_class}" id="layout_${childNavigationEntry.getLayoutId()}"role="presentation">
																							<a class="${child_link_css_class}" href="${childNavigationEntry.getRegularURL(request)}" role="menuitem">
																								${childNavigationEntry.getName(locale)}
																							</a>
																						</li>
																					</#if>
																				</#list>
																			</#if>
																		</ul>
																	</div>
																</div>
															</#if>

														</li>
													</#if>
												</#if>

											</#if>
										</#list>
									</#if>
								</ul>
							</nav>

							<div class="account-links hidden-xs" role="region" aria-label="Account">

								<#if is_signed_in>
									<a class="log-in-link" href="${account_header_link}"><@clay["icon"] symbol="user" /><span>Your account</span></a>
									<#if digitalPlacePermissionService.hasSiteAdminAccess(page_group, theme_display.getPermissionChecker()) >
										<@liferay.user_personal_bar />
									<#else>
										| <a class="log-in-link" href="${theme_display.getURLSignOut()}"><@liferay.language key="sign-out" /></a>
									</#if>
								<#elseif account_header_link??>
									<a class="log-in-link" href="${account_header_link}"><@clay["icon"] symbol="user" /><span>Log in to my account</span></a>
								</#if>
							</div>
						</div>
					</#if>

				</div>
			</div>

		</div>
	</#if>

	<div id="header-top" class="primary-background primary-font-color">

		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<#if show_breadcrumbs>
						<nav id="breadcrumbs" aria-label="breadcrumbs"><@liferay.breadcrumbs /></nav>
					</#if>
				</div>
			</div>	
			<div class="row title-search-container">
				<div class="col-md-8">

					<#if show_site_name>
						<div class="site-title">

							<#if layout.getName(locale)?matches(publicLayouts[0].getName(locale), "i")>
								<span class="title">${site_name}</span>
							<#else>
								<span class="title-page">${site_name}</span>
							</#if>
						</div>
					<#else>
						<div class="site-logo">
							<a class="${logo_css_class} header-logo" href="${site_default_public_url}" title="<@liferay.language_format arguments="${site_name}" key="go-to-x" />">
								<img alt="${logo_description}" src="${site_logo}" />
							</a>
						</div>
					</#if>

					<#if show_navigation>
						<div class="d-md-none">
							<div class="account-links" role="region" aria-label="account">
								<#if account_header_link??>
									<a href="${account_header_link}">Your account</a>
								</#if>
								<#if is_signed_in>
									<#if digitalPlacePermissionService.hasSiteAdminAccess(page_group, theme_display.getPermissionChecker()) >
										<@liferay.user_personal_bar />
									<#else>
										| <a href="${theme_display.getURLSignOut()}"><@liferay.language key="sign-out" /></a>
									</#if>
								</#if>
							</div>
						</div>
					</#if>

				</div>

				<div class="col-md-4">

					<@liferay.search_bar default_preferences="${preferences}" />

				</div>

			</div>
		</div>

	</div>

</header>
