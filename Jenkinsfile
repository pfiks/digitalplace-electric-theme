#!groovy

pipeline {

	agent {
		label 'docker.local.jenkins.slave.jdk11'
	}

	stages {

		stage("Build") {

			steps {
				sh "./gradlew gulpBuild"
			}

		}

		stage("Publish") {

			when {
				branch pattern: "master/*"
			}

			steps {
				sh "./gradlew artifactoryPublish"
			}

		}

	}

	post {

		fixed {

			script {

				if (env.BRANCH_NAME.startsWith('master/')) {

					slackSend channel: '#build-issues',
							color: "good",
							message: "*${currentBuild.currentResult}:* ${env.JOB_NAME} build ${env.BUILD_NUMBER}\n More info at: ${env.BUILD_URL}"

				}

			}

		}

		regression {

			script {

				if (env.BRANCH_NAME.startsWith('master/')) {

					slackSend channel: '#build-issues',
							color: "danger",
							message: "*${currentBuild.currentResult}:* ${env.JOB_NAME} build ${env.BUILD_NUMBER}\n More info at: ${env.BUILD_URL}"

				}

			}

		}

	}

}
